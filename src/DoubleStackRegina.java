import java.util.LinkedList;
import java.util.Objects;

public class DoubleStackRegina {
  private final LinkedList<Double> elements;
  public static void main(String[] args) {}

  DoubleStackRegina() {
    elements = new LinkedList<>();
  }

  private boolean isRightSymbol(String symbol) {
    String actions = "+, -, *, /, SWAP, ROT, DUP";
    return actions.contains(symbol);
  }

  @Override
  public Object clone() {
    DoubleStackRegina cloned = new DoubleStackRegina();

    if (elements.size() != 0) {
      for (double element : elements) {
        cloned.push(element);
      }
    }

    return cloned;
  }

  public boolean stEmpty() {
    return elements.size() == 0;
  }

  public void push(double a) {
    elements.add(a);
  }

  public double pop() {
    if (!stEmpty()) {
      double lastElement = elements.getLast();
      elements.remove(elements.size() - 1);
      return lastElement;
    }

    throw new IndexOutOfBoundsException("No elements left!");
  }

  public void op(String s) {
    if (!isRightSymbol(s)) {
      throw new IllegalArgumentException("Unknown operator " + s);
    }

    if (elements.size() < 2 && !s.equals("DUP")) {
      throw new IndexOutOfBoundsException("You need two elements to do operation " + s);
    }

    if ("SWAP".equals(s)) {
      this.swap();
    } else if ("ROT".equals(s)) {
      this.rot();
    } else if ("DUP".equals(s)) {
      this.dup();
    } else {
      double el2 = pop();
      double el1 = pop();

      if ("+".equals(s)) {
        this.push(el1 + el2);
      } else if ("-".equals(s)) {
        this.push(el1 - el2);
      } else if ("*".equals(s)) {
        this.push(el1 * el2);
      } else if ("/".equals(s)) {
        this.push(el1 / el2);
      }
    }
  }

  public double tos() {
    if (!stEmpty()) {
      return elements.getLast();
    }
    throw new IndexOutOfBoundsException("No elements left!");
  }

  @Override
  public boolean equals(Object o) {
    if (o.getClass() != DoubleStackRegina.class) {
      return false;
    } else if (((DoubleStackRegina) o).elements.size() != elements.size()) {
      return false;
    }

    for (int i = 0; i < elements.size(); i++) {
      if (!Objects.equals(((DoubleStackRegina) o).elements.get(i), elements.get(i))) {
        return false;
      }
    }

    return true;
  }

  @Override
  public String toString() {
    if (stEmpty()) {
      return "The stack is empty.";
    }

    StringBuilder s = new StringBuilder();
    for (double element : elements) {
      s.append(element).append(" ");
    }

    return s.toString();
  }

  public void swap() {
    if (this.elements.size() < 2) {
      throw new IllegalStateException("You need at least two numbers!");
    }

    double newSecond = this.pop();
    double newLast = this.pop();

    this.push(newSecond);
    this.push(newLast);
  }

  public void dup() {
    if (!this.stEmpty()) {
      double copied = this.tos();
      this.push(copied);
    } else {
      throw new IllegalStateException("You need to add an element to duplicate!");
    }
  }

  public void rot() {
    if (this.elements.size() < 3) {
      throw new IllegalStateException("You need at least 3 elements to do this operation!");
    }

    double thirdElement = this.elements.get(elements.size() - 3);
    this.elements.remove(elements.size() - 3);
    this.push(thirdElement);
  }

  public static double interpret(String pol) {
    DoubleStackRegina stack = new DoubleStackRegina();
    String[] chars = pol.trim().split("\\s+");

    if (chars.length == 0) {
      throw new RuntimeException("A given string -" + pol + "- is empty!");
    } else if (chars.length == 1) {
      return Double.parseDouble(chars[0]);
    }

    for (String symbol : chars) {
      try {
        stack.push(Double.parseDouble(symbol));
      }
      catch (NumberFormatException e) {
        if (stack.isRightSymbol(symbol)) {
          try {
            stack.op(symbol);
          }
          catch (IndexOutOfBoundsException ioobx) {
            throw new IndexOutOfBoundsException("Too many operators in " + pol);
          }
          catch (IllegalStateException illegalStateException) {
            throw new IllegalStateException(illegalStateException.getMessage() + " Input was: " + pol);
          }
        } else {
          throw new IndexOutOfBoundsException("Unknown operator in " + pol);
        }
      }
    }

    if (stack.elements.size() != 1) {
      throw new IndexOutOfBoundsException("Too many numbers in " + pol);
    }
    return stack.tos();
  }
}
