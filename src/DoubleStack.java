import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Double.parseDouble;
import static java.lang.String.format;
import static java.math.BigDecimal.valueOf;
import static java.math.RoundingMode.CEILING;

public class DoubleStack {
  private final LinkedList<Double> linkedList;

  public static void main(String[] args) {
  }

  DoubleStack() {
    this.linkedList = new LinkedList<>();
  }

  private DoubleStack(DoubleStack doubleStack) {
    this.linkedList = new LinkedList<>(doubleStack.linkedList);
  }

  @Override
  public Object clone() throws CloneNotSupportedException {
    return new DoubleStack(this);
  }

  public boolean stEmpty() {
    return linkedList.isEmpty();
  }

  public double tos() {
    if (linkedList.isEmpty()) {
      throw new RuntimeException("Stack is empty, cannot read from top!");
    }
    return linkedList.getLast();
  }

  public void push(double a) {
    linkedList.addLast(a);
  }

  public double pop() {
    if (linkedList.isEmpty()) {
      throw new RuntimeException("Stack is empty, cannot pop element!");
    }
    return linkedList.removeLast();
  }

  private void swap() {
    if (linkedList.size() < 2) {
      throw new IllegalArgumentException("Cannot swap if there are less than two arguments!");
    }

    Double secondOperand = linkedList.removeLast();
    Double firstOperand = linkedList.removeLast();

    linkedList.addLast(secondOperand);
    linkedList.addLast(firstOperand);
  }

  public void op(String operator) {
    if (!isValidMathOperator(operator)) {
      throw new IllegalArgumentException(format("%s is not a mathematical operator", operator));
    }

    if (linkedList.size() < 2) {
      throw new IllegalArgumentException(format("Stack size is less than two, cannot perform calculation. Remaining stack was: %s; Operator was: %s", linkedList, operator));
    }

    BigDecimal secondOperand = valueOf(linkedList.removeLast());
    BigDecimal firstOperand = valueOf(linkedList.removeLast());

    switch (operator) {
      case "+":
        linkedList.addLast(firstOperand.add(secondOperand).doubleValue());
        break;
      case "-":
        linkedList.addLast(firstOperand.subtract(secondOperand).doubleValue());
        break;
      case "/":
        linkedList.addLast(firstOperand.divide(secondOperand, 2, CEILING).doubleValue());
        break;
      case "*":
        linkedList.addLast(firstOperand.multiply(secondOperand).doubleValue());
        break;
    }
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof DoubleStack)) {
      return false;
    }

    DoubleStack incomingStack = (DoubleStack) o;

    if (incomingStack.linkedList.size() != linkedList.size()) {
      return false;
    }

    for (int i = 0; i < linkedList.size(); i++) {
      if (!Objects.equals(linkedList.get(i), incomingStack.linkedList.get(i))) {
        return false;
      }
    }

    return true;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();

    for (Double aDouble : linkedList) {
      builder.append(aDouble);
    }

    return builder.toString();
  }

  public static double interpret(String pol) {
    checkIfInputIsNullOrEmpty(pol);
    String[] splitArguments = pol.trim().split("[\\s]+");
    checkIfAllArgumentsAreValid(splitArguments, pol);

    if (splitArguments.length == 1 && isNumeric(splitArguments[0])) {
      return parseDouble(splitArguments[0]);
    }

    checkArgumentOrder(splitArguments, pol);

    return calculateEndResult(splitArguments);
  }

  private static double calculateEndResult(String[] splitArguments) {
    DoubleStack stack = new DoubleStack();

    for (String splitArgument : splitArguments) {
      if (isNumeric(splitArgument)) {
        stack.push(parseDouble(splitArgument));
      } else if (isStackOperation(splitArgument)) {
        stack.performStackOperation(splitArgument);
      } else {
        stack.op(splitArgument);
      }
    }

    return stack.tos();
  }

  private void performStackOperation(String splitArgument) {
    if ("SWAP".equals(splitArgument)) {
      this.swap();
    } else if ("DUP".equals(splitArgument)) {
      this.duplicate();
    } else {
      this.rotate();
    }
  }

  private void rotate() {
    if (linkedList.size() < 3) {
      throw new IllegalArgumentException("Cannot rotate because there are not enough arguments");
    }

    Double firstFromTop = linkedList.removeLast();
    Double secondFromTop = linkedList.removeLast();
    Double thirdFromTop = linkedList.removeLast();

    linkedList.addLast(secondFromTop);
    linkedList.addLast(firstFromTop);
    linkedList.addLast(thirdFromTop);
  }

  private void duplicate() {
    linkedList.addLast(linkedList.getLast());
  }

  private static boolean isStackOperation(String operator) {
    return "SWAP".equals(operator) || "DUP".equals(operator) || "ROT".equals(operator);
  }

  private static void checkArgumentOrder(String[] splitArguments, String pol) {
    int numOfArgs = splitArguments.length;

    if (isNumeric(splitArguments[numOfArgs - 1])) {
      throw new IllegalArgumentException(format("Too many numbers in input! Input was: %s", pol));
    }

    if (isStackOperation(splitArguments[numOfArgs - 1])) {
      throw new IllegalArgumentException(format("Input cannot end with stack operation! Input was: %s", pol));
    }

    if (numOfArgs == 2) {
      throw new IllegalArgumentException(format("Input has only two arguments! Input was: %s", pol));
    }
  }

  private static void checkIfInputIsNullOrEmpty(String pol) {
    if (pol == null || pol.isEmpty()) {
      throw new IllegalArgumentException(format("Input cannot be null, empty or whitespace! Input was: %s", pol));
    }
  }

  private static void checkIfAllArgumentsAreValid(String[] splitArguments, String pol) {
    for (String splitArgument : splitArguments) {
      boolean isValidOperator = isValidMathOperator(splitArgument);
      boolean isNumeric = isNumeric(splitArgument);
      boolean isStackOperation = isStackOperation(splitArgument);

      if (!isValidOperator && !isNumeric && !isStackOperation) {
        throw new IllegalArgumentException(format("Input contains either a non-numeric or invalid math argument or stack operation %s! Input was: %s", splitArgument, pol));
      }
    }
  }

  private static boolean isValidMathOperator(String operator) {
    String validMathOperators = "[+-/*]";
    Pattern pattern = Pattern.compile(validMathOperators);
    Matcher matcher = pattern.matcher(operator);

    return matchesExactly(matcher);
  }

  private static boolean matchesExactly(Matcher matcher) {
    int matches = 0;
    while (matcher.find()) {
      matches++;
    }
    return matches == 1;
  }

  private static boolean isNumeric(String argument) {
    try {
      parseDouble(argument);
      return true;
    }
    catch (NumberFormatException e) {
      return false;
    }
  }
}
